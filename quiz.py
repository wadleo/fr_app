from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABcrKh8vy884t-YvAltuIFWu3fRCTbyvm8Rit6vG0hk6FedXblx16hzsk7tpHiHIozSAfnalJPM-w6yhDdSrQXtHdLPp6W9CsmqdXAo1EdKunY38gxrP3FAiYbDr975IMAJu0JCMqdf2SeVvBPPDUbBYabBf4lBWrWEABk4x__uG22GPjNvvyaXst33bR9UKVrbgZvX'

def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
