## IWS Feature Request App
The app allows a user to submit feature request and also shows a list of already submitted requests.

A "feature request" is a request for a new feature that will be added onto an existing piece of software. 

The fields are:

* Title: A short, descriptive name of the feature request.
* Description: A long description of the feature request.
* Client: A selection list of clients (use "Client A", "Client B", "Client C")
* Client Priority: A numbered priority according to the client (1...n). Client Priority numbers should not repeat for the given client, so if a priority is set on a new feature as "1", then all other feature requests for that client should be reordered.
* Target Date: The date that the client is hoping to have the feature.
* Product Area: A selection list of product areas (use 'Policies', 'Billing', 'Claims', 'Reports')

## Tech Stack
This project was built using the following technologies:

* OS: Ubuntu
* Server Side Scripting: Python 3.7
* Server Framework: Flask + Flask Marshmallow (RESTful API)
* Database: Mysql (RDS) / sqlite on local
* ORM: Sql-Alchemy
* JavaScript: KnockoutJS + Knockout Validation Js
* CSS: Bootstrap 4.

## Getting Started

The app is hosted live at [IWS Feature Request App Demo]() using AWS Elastic Beanstalk, but following the next steps will set it up locally.

These instructions will give you a local repository on your machine which you can then run and use. 
```
git clone https://github.com/wadleo/feature-requests.git
cd feature-requests
```

If you have docker installed;
```
docker build -t fr_app
docker run -d -p 8080:8080 fr_app
```

If you don't have docker installed, you will need to follow these (assuming you have python3 and python3-venv installed);
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py
python run.py
```

Look for the line 'Running on http://0.0.0.0:8080/'. If you navigate to that url.


To run the tests;
```
python3 tests.py
```

For your any questions, I'm always available at wadingaleonard@gmail.com(gmail & hangouts), wad_leo(skype) or on whatsapp +237679286569, thanks for reviewing my work.

