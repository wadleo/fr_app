# Import things from Flask that we need.
from flask import Flask, request, render_template, json, jsonify

# Import SqlAclchemy.
from flask_sqlalchemy import SQLAlchemy

# Import db
from fr_app import app, db

# Import datetime to calculate default date
from datetime import datetime

# Import our feature model
from fr_app.model import Feature

# Import our feature schema
from fr_app.schema import FeatureSchema

# schema for loading json data
feature_schema = FeatureSchema()

# utilities for features
from fr_app.utils import *

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/features')
def list_features():
    features = Feature.query.order_by(Feature.id.desc()).all()
    if len(features) > 0:
        features = feature_schema.dump(features, many=True).data
        return jsonify({'type':'data', 'content':features})
    else:
        return jsonify({'type':'msg', 'content':'No feature requests made.'})

@app.route('/features/show/<feature_id>')
def show_feature(feature_id):
    feature = Feature.query.filter_by(id=feature_id).first()
    if feature:
        feature = feature_schema.dump(feature).data
        return jsonify({'type':'data', 'content':feature})
    else:
        return jsonify({'type':'msg', 'content':'Sorry couldn\'t find the feature.'})

@app.route('/features/add', methods = ['POST'])
def create_feature():
    if request.method == 'POST':
        # Load data from request
        json_data = json.loads(json.dumps(request.form))
        if not json_data:
            return jsonify({'type':'msg', 'content':'No input data provided'})

        # validate and deserialize input
        data, errors = feature_schema.load(json_data)
        if errors:
            return jsonify({'type':'msg', 'content':errors})

        # fix priorities for request
        fix_requests_priorities(data.get('client'), data.get('client_priority'))

        # build feature object
        feature = Feature()
        feature = feature._build_feature_data_(data)
        db.session.add(feature)
        
        # persist all the changes to the features
        db.session.commit()

        # serialize data to json
        feature = feature_schema.dump(feature).data
        
        return jsonify({'type':'data', 'content':feature})
    else:
        return jsonify({'type':'msg', 'content':'Method not allowed'})

@app.route('/features/update/<feature_id>', methods = ['POST'])
def update_feature(feature_id):
    # find the feature
    feature = Feature.query.filter_by(id=feature_id).first()
    if not feature:
        return jsonify({'type':'msg', 'content':'Sorry couldn\'t find the policy'})

    if request.method == 'POST':
        # get the data from request
        json_data = json.loads(json.dumps(request.form))
        if not json_data:
            return jsonify({'type':'msg', 'content':'No input data provided'})
    
        # validate and deserialize input
        data, errors = feature_schema.load(json_data)
        if errors:
            return jsonify({'type':'msg', 'content':errors})

        # fix priorities for request
        fix_requests_priorities(data.get('client'), data.get('client_priority'))

        # build feature object
        feature = feature._build_feature_data_(data)

        # persist all the changes to the features
        db.session.commit()
            
        # serialize data to json
        feature = feature_schema.dump(feature).data
        
        return jsonify({'type':'data', 'content':feature})
    elif request.method == 'GET':
        return jsonify({'type':'msg', 'content':'Method not allowed.'})

@app.route('/features/delete/<feature_id>', methods = ['DELETE'])
def delete_feature(feature_id):
    # find the feature
    feature = Feature.query.filter_by(id=feature_id).first()
    if not feature:
        return jsonify({'type':'msg', 'content':'Sorry couldn\'t find the feature.'})

    if request.method == 'DELETE':
        db.session.delete(feature)
        db.session.commit()
        return jsonify({'type':'msg', 'content':'Deleted Feature Request #'+ feature_id})
    elif request.method == 'GET':
        return jsonify({'type':'msg', 'content':'Method not allowed.'})

