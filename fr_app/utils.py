from datetime import datetime
from fr_app.model import Feature
from marshmallow import ValidationError

def validate_future_date(target_date):
    """Validate date in future."""
    if target_date < datetime.now().date():
        raise ValidationError('The target date must be in the future')

def fix_requests_priorities(client, priority):
    """
    Fix client priority as per new client priority supplied by user at the time
    of creating a feature request.
    """

    # looks for the feature with this priority
    feature = Feature.query.filter(Feature.client == client) \
                    .filter(Feature.client_priority == priority).order_by('id').first()

    # keeps checking for conflicts
    if feature:
        # increment to the next priority
        priority += 1

        # check if this next priority has also been used and keep incrementing until it finds a free priority
        while check_priority_taken(client, priority):
            priority += 1

        # finally found a free value and assigns this feature to that priority
        feature.client_priority = priority

    return

def check_priority_taken(client, priority):
    feature = Feature.query.filter(Feature.client == client) \
                    .filter(Feature.client_priority == priority).order_by('id').first()

    if feature:
        return True
    else:
        return False
