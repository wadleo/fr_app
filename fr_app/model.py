from fr_app import db

class Feature(db.Model):
    __tablename__ = 'features'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(1024), nullable=False)
    client = db.Column(db.String(80), nullable=False)
    client_priority = db.Column(db.Integer, nullable=False)
    target_date = db.Column(db.Date, nullable=False)
    product_area = db.Column(db.String(80), nullable=False)

    def __init__(self, title=None, description=None, client=None, client_priority=None, \
        target_date=None, product_area=None):
        self.title = title
        self.description = description
        self.client = client
        self.client_priority = client_priority
        self.target_date = target_date
        self.product_area = product_area

    def _build_feature_data_(self, data):
        self.title = data.get('title')
        self.description = data.get('description')
        self.client = data.get('client')
        self.client_priority = data.get('client_priority')
        self.target_date = data.get('target_date')
        self.product_area = data.get('product_area')
        
        return self

    def __repr__(self):
        return '<Feature %r>' % self.title

    __str__ = __repr__