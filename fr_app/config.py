import os

# Show debug information on development
DEBUG = True

WTF_CSRF_ENABLED = False

SECRET_KEY = 'iws-secret-key'

# edit the URI below to add your RDS password and your AWS URL
# The other elements are the same as used in the tutorial
# format: (user):(password)@(db_identifier).amazonaws.com:3306/(db_name)

# For RDS database
# SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://iwsfr_app:iwsfr_app2019@iwsfeaturerequests.c2lfybpklii5.eu-west-3.rds.amazonaws.com:3306/iwsfr_app'

# For local database with sqlite
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.abspath("feature-request.sqlite")

SQLALCHEMY_ECHO = False

SQLALCHEMY_POOL_RECYCLE = 3600

SQLALCHEMY_TRACK_MODIFICATIONS = False
