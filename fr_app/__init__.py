# Loading necessary imports for the app
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# Initialize the application.
app = Flask(__name__)
app.config.from_pyfile('config.py')

# Initialize db and ma objects with SQLAlchemy and Marshmallow respectively
db = SQLAlchemy(app)
ma = Marshmallow(app)

# Import the views file for routing.
from . import views
