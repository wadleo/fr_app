from marshmallow import Schema, fields, validate
from .utils import validate_future_date

class FeatureSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(
        required=True,
        validate=[validate.Length(max=80)]
    )
    description = fields.Str(
        required=True,
        validate=[validate.Length(max=1024)]
    )
    client = fields.Str(
        required=True,
        validate=[validate.Length(max=80)]
    )
    client_priority = fields.Int(
        required=True,
        validate=[validate.Range(min=1)]
    )
    product_area = fields.Str(
        required=True,
        validate=[validate.Length(max=80)]
    )
    target_date = fields.Date(
        required=True,
        validate=[validate_future_date],
        error_messages={
            'null': {
                'message': 'Date should be in the format YYYY-MM-DD',
                'code': 400
            },
            'validator_failed': {
                'message': 'Date should be in the format YYYY-MM-DD and greater than today\'s date',
                'code': 400
            },
            'required': {
                'message': 'Target date is required in the format YYYY-MM-DD',
                'code': 400
            }
        }
    )
