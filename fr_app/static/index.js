// Activates knockoutjs validation
ko.validation.rules.pattern.message = 'Invalid.';

ko.bindingHandlers.trimText = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var trimmedText = ko.computed(function () {
            var untrimmedText = ko.utils.unwrapObservable(valueAccessor());
            var defaultMaxLength = 30;
            var minLength = 5;
            var maxLength = ko.utils.unwrapObservable(allBindingsAccessor().trimTextLength) || defaultMaxLength;
            if (maxLength < minLength) maxLength = minLength;
            var text = untrimmedText.length > maxLength ? untrimmedText.substring(0, maxLength - 1) + '...' : untrimmedText;
            return text;
        });
        ko.applyBindingsToNode(element, {
            text: trimmedText
        }, viewModel);

        return {
            controlsDescendantBindings: true
        };
    }
};

ko.validation.init({
  registerExtenders: true,
  messagesOnModified: true,
  insertMessages: true,
  parseInputAttributes: true,
  messageTemplate: null,
  errorClass: 'show-error',
}, true);

function FeatureRequestViewModel() {
  var self = this;
  self.feature = ko.observable();
  self.features = ko.observableArray();

  // some static data
  clients = ko.observable([
    {name: "Client A", id: "A"},
    {name: "Client B", id: "B"},
    {name: "Client C", id: "C"}
  ]);

  product_areas = ko.observable([
    {name: "Policies", id: "Policies"},
    {name: "Billings", id: "Billings"},
    {name: "Claims", id: "Claims"},
    {name: "Reports", id: "Reports"}
  ]);

  // add form properties
  self.feature_id = ko.observable();
  self.title = ko.observable().extend({required : true});
  self.client = ko.observable().extend({required : true});
  self.description = ko.observable().extend({required : true});
  self.client_priority = ko.observable().extend({required : true});
  self.target_date = ko.observable().extend({required : true});
  self.product_area = ko.observable().extend({required : true});

  // for toggling the view as it's an SPA
  self.default_action = ko.observable('saveFeature');

  // fetches all the list of features
  self.loadFeatures = function() {
    $.ajax('/features', {
      success: function(result) {
        if(result['type'] == 'data') {
          self.features(result['content']);
        } else {
          alert(result['content']);
        }
      }
    });
  }

  // display all feature
  self.showFeature = function(feature) {
    $.ajax('/features/show/' + feature.id, {
      type: "GET",
      contentType: "application/json",
      success: function(result) {
        if(result['type'] == 'msg') {
          alert(result['content']);
        }  else {
          self.feature(result['content']);
        }
      }
    });
  }

  // decide which method to either create or update a feature
  self.saveAEForm = function() {
    if(self.default_action() == 'saveFeature') {
      self.saveFeature();
    } else if(self.default_action() == 'updateFeature') {
      self.updateFeature(self.feature_id());
    }
  }

  // save a new feature
  self.saveFeature = function() {
    self.default_action('saveFeature');

    // validating the required fields of a feature
    if (self.errors().length !== 0) {
      self.errors.showAllMessages();
      return;
    }

    $.post('/features/add', {
        'title': self.title(),
        'description': self.description(),
        'client': self.client(),
        'client_priority': self.client_priority(),
        'target_date': self.target_date(),
        'product_area': self.product_area(),
      }, function(result) {
        console.log(result);
        if(result['type'] == 'msg') {
          alert(JSON.stringify(result['content']));
        } else {
          $('#addFeatureModal').modal('toggle');
          alert('The feature has been successfully added.');
          self.loadFeatures();
          self.feature(result['content']);
        }
      }
    );
  }

  // load the form with the selected feature data
  self.editFeature = function(feature) {
    self.feature_id(feature.id);
    self.title(feature.title);
    self.description(feature.description);
    self.client(feature.client);
    self.client_priority(feature.client_priority);
    self.product_area(feature.product_area);
    self.target_date(feature.target_date);
    self.default_action('updateFeature');
    $('#addFeatureModal').modal('toggle');
  }

  // update a new feature
  self.updateFeature = function(feature_id) {
    self.default_action('updateFeature');

    // validating the required fields of a feature
    if (self.errors().length !== 0) {
      self.errors.showAllMessages();
      return;
    }

    var data = new Object(); 
    data.title = self.title();
    data.description = self.description();
    data.client = self.client();
    data.client_priority = self.client_priority();
    data.target_date = self.target_date();
    data.product_area = self.product_area();

    $.post('/features/update/'+feature_id, data, function(result) {
        console.log(result);
        if(result['type'] == 'msg') {
          alert(JSON.stringify(result['content']));        
        } else {
          self.feature(result['content']);
          $('#addFeatureModal').modal('toggle');
          alert('The feature has been successfully updated.');
          self.loadFeatures();
        }
      }
    );
  }

  // delete a particular feature
  self.deleteFeature = function(feature) {
    if(confirm("Are you sure you wish to delete this feature (This action is irreversible)?")) {
      $.ajax('/features/delete/'+feature.id, {
        type: "DELETE",
        contentType: "application/json",
        success: function(result) {
          if(result['msg'] == 'msg') {
            alert(result['content']);
            self.loadFeatures();
          }
        },
        error: function(response, status, message) {
          alert(message);
        }
      });
    }
  }

  // fetch load of features
  self.loadFeatures();
}

//setting up the viewModel with validation
viewModel = new FeatureRequestViewModel();

viewModel.errors = ko.validation.group(viewModel);

// Activates knockout.js
ko.applyBindings(viewModel);
