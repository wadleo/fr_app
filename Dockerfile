FROM ubuntu:18.10

LABEL maintainer="Wadinga Leonard N. <wadingaleonard@gmail.com>"

RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip nginx
RUN pip3 install uwsgi

COPY . /feature-requests
WORKDIR /feature-requests

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]
CMD ["run.py"]
