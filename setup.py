from fr_app import db
from datetime import datetime
from fr_app.model import Feature

db.create_all()

# add demo features
feature1 = Feature('Overdued Bills', 'Managed bills', 'Client A', 1, \
	datetime.strptime('2019-04-10', '%Y-%m-%d'), 'Billings')
feature2 = Feature('Old Claims', 'Still unpaid', 'Client B', 5, \
	datetime.strptime('2019-04-10', '%Y-%m-%d'), 'Claims')
feature3 = Feature('Compare Policies', 'To compare policies', 'Client C', 3, \
	datetime.strptime('2019-04-10', '%Y-%m-%d'), 'Policies')

db.session.add(feature1)
db.session.add(feature2)
db.session.add(feature3)
db.session.commit()

print("DB created.")