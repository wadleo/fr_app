import os
import json
import unittest
import tempfile

from copy import deepcopy
from datetime import datetime

from dateutil.relativedelta import relativedelta

from fr_app import app, db
from fr_app.model import Feature

# setting environment to testing
app.config['TESTING'] = True
app.config['WTF_CSRF_ENABLED'] = False

# setting database to sqlite in memory
app.config['SQLALCHEMY_'] =  True
app.config['SQLALCHEMY_DATABASE_URI'] =  'sqlite:///:memory:'

class FeatureTestCase(unittest.TestCase):
    app, db = app, db
    post_data = {
        'id': 1,
        'client': 'Client A',
        'client_priority': 1,
        'product_area': 'Billing',
        'title': 'A test feature request title',
        'description': 'A test feature request description',
        'target_date': str(datetime.utcnow().date())
    }

    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.testing = True
        self.app = app.test_client()

        with app.app_context():
            db.create_all()

    def tearDown(self):
        db.drop_all()
        os.close(self.db_fd)
        os.unlink(app.config['DATABASE'])


    # list features tests
    def test_feature_list_with_data(self):
        # testing the list all features method with data
        post_data = deepcopy(self.post_data)
        for turn in range(1, 5):
            post_data['id'] = turn
            post_data['client_priority'] = turn
            response = self.app.post(
                '/features/add',
                data=post_data
            )
            response_data = json.loads(response.data)

        response = self.app.get('/features')
        response_data = json.loads(response.data)
        features = response_data['content']
        assert response_data['type'] == 'data'
        assert len(features) == Feature.query.count()

    def test_feature_list_no_data(self):
        # testing the list all features method with data
        response = self.app.get('/features')
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content'] == 'No feature requests made.'


    # show feature tests
    def test_feature_show_with_data(self):
        # testing the show all features method with data
        post_data = deepcopy(self.post_data)
        
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)

        response = self.app.get('/features/show/1')
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

    def test_feature_show_no_data(self):
        # testing the show all features method with data
        response = self.app.get('/features/show/1')
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content'] == 'Sorry couldn\'t find the feature.'


    # create method tests
    def test_creating_feature_get_method(self):
        # testing create method with get method
        response = self.app.get('/features/add', data=dict())
        assert response.status_code == 405

    def test_creating_feature_no_data(self):
        # testing create method with no data
        response = self.app.post('/features/add', data=dict())
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content'] == 'No input data provided'

    def test_creating_feature_valid_data(self):
        # testing create method with valid data
        response = self.app.post(
            '/features/add',
            data=self.post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        self.assertDictEqual(response_data['content'], self.post_data)

    def test_creating_feature_no_title(self):
        # testing create method with no title
        post_data = deepcopy(self.post_data)
        post_data.pop('title')

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['title'][0] == 'Missing data for required field.'

    def test_creating_feature_invalid_title(self):
        # testing api with very long title above 80 characters
        post_data = deepcopy(self.post_data)
        post_data['title'] = "Very long title so it fails and we assert accordingly. \
                                Very long title so it fails and we assert accordingly"

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['title'][0] == 'Longer than maximum length 80.'

    def test_creating_feature_no_description(self):
        # testing create method with no description
        post_data = deepcopy(self.post_data)
        post_data.pop('description')

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['description'][0] == 'Missing data for required field.'

    def test_creating_feature_no_client_field(self):
        # testing the create method with no client field
        post_data = deepcopy(self.post_data)
        del post_data['client']

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['client'][0] == \
            'Missing data for required field.'

    def test_creating_feature_past_target_date(self):
        # testing the create method with a past date
        yesterday = relativedelta(days=-1) + datetime.utcnow().date()
        post_data = deepcopy(self.post_data)
        post_data['target_date'] = str(yesterday)

        response = self.app.post(
            '/features/add',
            data=dict(post_data)
        )
        response_data = json.loads(response.data)
        assert response_data['content']['target_date'][0] == \
            'The target date must be in the future'

    def test_creating_feature_invalid_target_date(self):
        # testing the create method with an invalid target date
        post_data = deepcopy(self.post_data)
        # invalid date for the month September
        post_data['target_date'] = '2021-09-31'
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['target_date'][0] ==\
            'Not a valid date.'

    def test_creating_feature_no_product_area_field(self):
        # testing the create method with product area field
        post_data = deepcopy(self.post_data)
        del post_data['product_area']

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['product_area'][0] == \
            'Missing data for required field.'

    def test_creating_feature_negative_client_priority(self):
        # testing the create method with negative client priority
        post_data = deepcopy(self.post_data)
        post_data['client_priority'] = -1
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content']['client_priority'][0] == \
            'Must be at least 1.'

    def test_creating_feature_check_client_priority_reordering(self):
        # testing create method on reordering on creating a new feature with already existing priority
        post_data = deepcopy(self.post_data)

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        first_id, first_client_priority = \
            response_data['content']['id'],\
            response_data['content']['client_priority']
        assert first_id == 1
        assert first_client_priority == 1

        # sending same client_priority again will result in moving the first
        # priority to 2
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        # update the id of the post_data because it will be update to the next value
        post_data['id'] += 1
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        second_id, second_client_priority = \
            response_data['content']['id'],\
            response_data['content']['client_priority']

        first_fr = Feature.query.get(first_id)
        assert first_fr.id == 1
        # got reordered after adding another feature request
        assert first_fr.client_priority == 2
        assert second_id == 2
        assert second_client_priority == 1

    def test_creating_feature_check_client_priority_no_reordering(self):
        # testing the create method when client priority needs no reordering
        post_data = deepcopy(self.post_data)

        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        first_id, first_client_priority = \
            response_data['content']['id'],\
            response_data['content']['client_priority']
        assert first_id == 1
        assert first_client_priority == 1

        post_data = deepcopy(self.post_data)
        post_data['client_priority'] = 7
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)

        # update the id of the post_data because it will be update to the next value
        post_data['id'] += 1
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        second_id, second_client_priority = \
            response_data['content']['id'],\
            response_data['content']['client_priority']

        first_fr = Feature.query.get(first_id)
        assert first_fr.id == 1
        # got reordered after adding another feature request
        assert first_fr.client_priority == 1
        assert second_id == 2
        assert second_client_priority == 7


    # update method tests
    def test_updating_feature(self):
        # testing the update method by updating some fields of a feature
        post_data = deepcopy(self.post_data)
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        description = response_data['content']['description']
        assert description == post_data['description']

        # update
        post_data['client'] = 'Client B'
        post_data['description'] = 'The updated description of the feature'
        response = self.app.post(
            '/features/update/1',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content']['description'] ==\
            post_data['description']

    def test_updating_feature_no_priority_reordering(self):
        # testing the update method by updating a client priority which has not been taken
        post_data = deepcopy(self.post_data)
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        client_priority = response_data['content']['client_priority']
        assert client_priority == post_data['client_priority']

        # update FR now
        post_data['client_priority'] = 5
        response = self.app.post(
            '/features/update/1',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content']['client_priority'] ==\
            post_data['client_priority']

    def test_updating_feature_client_priority_with_reordering(self):
        # testing update method with priority reordering
        post_data = deepcopy(self.post_data)
        for turn in range(1, 3):
            post_data['id'] = turn
            post_data['client_priority'] = turn
            response = self.app.post(
                '/features/add',
                data=post_data
            )
            response_data = json.loads(response.data)
            assert response_data['type'] == 'data'
            assert response_data['content'] == post_data

        # update client priority of one already taken
        post_data['client_priority'] = 1
        response = self.app.post(
            '/features/update/2',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content']['client_priority'] ==\
            self.post_data['client_priority']
        assert Feature.query.get(1).client_priority == 3
        assert Feature.query.get(2).client_priority == 1


    # delete method tests
    def test_deleting_feature_get_method(self):
        # testing delete method with get method
        response = self.app.get('/features/add', data=dict())
        assert response.status_code == 405

    def test_deleting_feature(self):
        # testing the delete method
        post_data = deepcopy(self.post_data)
        response = self.app.post(
            '/features/add',
            data=post_data
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'data'
        assert response_data['content'] == post_data

        # delete feature
        response = self.app.delete(
            '/features/delete/'+str(post_data['id'])
        )
        response_data = json.loads(response.data)
        assert response_data['type'] == 'msg'
        assert response_data['content'] == 'Deleted Feature Request #'+str(post_data['id'])

if __name__ == '__main__':
    unittest.main()
